#include<iostream>
#include<algorithm>
#include<vector>
#include<ncurses.h>
#include "afficheur.h"
using namespace std;

//Format:
//-blank line-
//h1 h2 m1 m2 s1 s2
//a1 a2 a3 a4 o1 o2 j1 j2;
//
//abcdefg ->
// aa
//b  c
//b  c
// dd
//e  f
//e  f
// gg
const int sz_1 = 4;
const int sz_2 = 1;

const int wdth = (2+sz_1+1)*6 + 3*2 + (2+sz_2+1) * 4 + 2;
const int hgth = std::max<int>(3+2*sz_1, 1 + 2*(3+2*sz_2));

const chtype block = ' ' | A_REVERSE;

WINDOW* a_win = nullptr; 

const int dig_s_c = 7;
const struct {int ofs[2]; chtype c;} small_dg[dig_s_c] =
	{
		{{1,0}, '-'},
		{{0,1}, '|'},
		{{2,1}, '|'},
		{{1,2}, '-'},
		{{0,3}, '|'},
		{{2,3}, '|'},
		{{1,4}, '-'}
	};
const int daf_xofs[8] = {0, 4, 8, 16, 0, 4, 8, 16};

void afficher_small_digit(int l[], bool* d){
	for(int i(0); i<dig_s_c; i++){
		unsigned y = l[1]+small_dg[i].ofs[1],
				 x = l[0]+small_dg[i].ofs[0];
		char c = d[i] ? small_dg[i].c : ' ';
		mvwaddch(a_win, y, x, c);
	}
}

void afficher_hline(int x, int y, int len, bool b){
	chtype c(b ? block : ' ');
	for(int i(0); i<len; ++x, ++i)
		mvwaddch(a_win, y, x, c);
}
void afficher_vline(int x, int y, int len, bool b){
	chtype c(b ? block : ' ');
	for(int i(0); i<len; ++y, ++i)
		mvwaddch(a_win, y, x, c);
}
void afficher_cnt(int x, int y, bool b){
	mvwaddch(a_win, y, x, b ? block : ' ');
}

void afficher_big_digit(int l[], bool *d, int sz){
	int x = l[0], y = l[1];
	afficher_hline(x+1, 	y, 		sz, d[0]);
	afficher_vline(x, 		y+1, 	sz,	d[1]);
	afficher_vline(x+sz+1,  y+1, 	sz, d[2]);
	afficher_hline(x+1, 	y+sz+1, sz, d[3]);
	afficher_vline(x, 		y+sz+2, sz, d[4]);
	afficher_vline(x+sz+1,  y+sz+2, sz, d[5]);
	afficher_hline(x+1,  y+2*(sz+1),sz, d[6]);
	afficher_cnt(x,			y,		d[0]||d[1]);
	afficher_cnt(x+sz+1,	y,		d[2]||d[1]);
	afficher_cnt(x,		y+2*(sz+1),	d[4]||d[6]);
	afficher_cnt(x+sz+1,y+2*(sz+1),	d[5]||d[6]);
	afficher_cnt(x, 	 	y+sz+1, d[1]||d[3]||d[4]);
	afficher_cnt(x+sz+1, 	y+sz+1, d[2]||d[3]||d[5]);
}

void afficher_big_d2(int l[], bool* d, int sz){
	for(unsigned i(0); i<2; ++i){
		afficher_big_digit(l, d, sz);
		d += 7;
		l[0] += sz + 3;
	}
}

void afficher(bool lns[][64]){
	int l[2] = {0,0};
	bool* dt = lns[0];
	for(unsigned i(0); i<6;++i){
		afficher_big_digit(l, dt, sz_1);
		l[0] += sz_1 + 3 + ((i&1)<<1);
		dt   += 7;
	}
	mvwaddch(a_win, sz_1, 2 * (sz_1 + 3),       ACS_BULLET);
	mvwaddch(a_win, sz_1+2, 2 * (sz_1 + 3),     ACS_BULLET);
	mvwaddch(a_win, sz_1, 4 * (sz_1 + 3) + 2,   ACS_BULLET);
	mvwaddch(a_win, sz_1+2, 4 * (sz_1 + 3) + 2, ACS_BULLET);

	int lft(l[0]);
	dt = lns[1];
	afficher_big_d2(l, dt + 3*14, sz_2);
	mvwaddch(a_win, sz_2+1, l[0], ACS_BULLET);
	l[0] += 2;
	afficher_big_d2(l, dt + 2*14, sz_2);
	l[0] = lft + 1;
	l[1] += sz_2 * 2 + 4;
	afficher_big_d2(l, dt, 		  sz_2);
	afficher_big_d2(l, dt+14, 	  sz_2);
	wrefresh(a_win);
}

void afficheur_init(){
	initscr();
	noecho();
	curs_set(0);
	int s_h, s_w;
	getmaxyx(stdscr, s_h, s_w);
	a_win = newwin(hgth,wdth, (s_h-hgth)/2, (s_w-wdth)/2); 
}

void afficheur_end(){
	delwin(a_win);
	endwin();
}
