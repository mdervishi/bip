#include<time.h>
#include<fstream>
#include<string>
#include<sstream>
#include<iostream>

#include "afficheur.h"

const unsigned int set_cycles = 50;

timespec sleep_time({0, 1000000});
unsigned min_steps = 100;

bool v_max = false;
bool custom_date = false;
struct{unsigned int a1,a2,o,j,h,m,s;} custd;
long long int nb_steps = ~0;

typedef unsigned long long int v_type;

extern v_type var_input;
extern v_type var_out_seg1;
extern v_type var_out_seg2;
extern "C" void simul_step();

v_type cur_input = 0;
//fonction appelée par simul_step
extern "C" void get_inputs() {
	var_input = cur_input;
}

inline void add_d2(v_type& s, v_type d2){
	s = (s<<8) | d2;
}

v_type encode_date(tm t){
	v_type rt(0);
	if(custom_date){
		add_d2(rt, custd.a1);
		add_d2(rt, custd.a2);
		add_d2(rt, custd.o);
		add_d2(rt, custd.j);
		add_d2(rt, custd.h);
		add_d2(rt, custd.m);
		add_d2(rt, custd.s);
	} else {
		add_d2(rt, 19 + t.tm_year / 100);
		add_d2(rt, t.tm_year % 100);
		add_d2(rt, t.tm_mon + 1);
		add_d2(rt, t.tm_mday);
		add_d2(rt, t.tm_hour);
		add_d2(rt, t.tm_min);
		add_d2(rt, t.tm_sec);
	}
	return rt << 8;
}

v_type last_o1 = 0;
v_type last_o2 = 0;

void int2line(bool* rt, v_type n){
	for(size_t i(7*8); i; n=n>>1) rt[--i] = n&1;
}

bool lines[2][64];

unsigned last_aff = 0;

void do_step(v_type c_in){
	cur_input = c_in;
	simul_step();
	last_aff++;
	if(last_o1 != var_out_seg1 || last_o2 != var_out_seg2){
		last_o1 = var_out_seg1;
		last_o2 = var_out_seg2;
		if (last_aff > 10000 || !v_max){
			last_aff = 0;
			int2line(lines[0], last_o1);
			int2line(lines[1], last_o2);
			afficher(lines);
		}
	}
}

int main(int argc, char** argv){
	for(int i(1);i<argc;++i){
		std::istringstream iss((std::string)argv[i]);
		std::string n;
		iss >> n;
		if(n == "vmax")
			v_max=true;
		else if(n == "date"){
			custom_date = true;
			unsigned int a;
			iss >> a >> custd.o >> custd.j >> custd.h >> custd.m >> custd.s;
			custd.a1 = a/100;
			custd.a2 = a%100;
		} else if(n == "sleep")
			iss >> sleep_time.tv_nsec;
		else if(n == "steps")
			iss >> nb_steps;
		else{
			std::cerr << "option inconnue: "<<n<<"\n";
			return 1;
		}
	}

	afficheur_init();
	//Initialisation
	while(!last_o1) do_step(0);

	//Set date
	time_t lt(time(nullptr));
	tm ini_tm;
	localtime_r((const time_t*)&lt, &ini_tm);
	v_type e_ini(encode_date(ini_tm));
	for(unsigned i(0); i<set_cycles; ++i)
		do_step(e_ini);
	
	//Fonctionnent
	unsigned long sig_todo(0);
	unsigned long last_sig(min_steps);
	for(long long int step(0); !~nb_steps || step < nb_steps; ++step){
		time_t ct(time(nullptr));
		if(ct > lt){
			sig_todo += ct - lt;
			lt = ct;
		}
		if(v_max)
			do_step(1);
			else {
				if(sig_todo && last_sig >= min_steps){
				sig_todo--;
				last_sig = 0;
				do_step(1);
			} else {
				do_step(0);
				last_sig ++;
			}
			nanosleep(&sleep_time, nullptr);
		}
	}

	afficheur_end();
	return 0;
}
