#!/bin/bash

riskv_as="riscv64-linux-gnu-as"
riskv_objcopy="riscv64-linux-gnu-objcopy"
config_file="tool/config.sh"

if [[ -a "tool/config.sh" ]] ; then
	. "$config_file"
fi

src="$1"
dst="$2"
"$riskv_as" "$1" -o "tool/prg.out"
"$riskv_objcopy" "tool/prg.out" -O binary
./tool/bin2rom.exe "tool/prg.out" "$2"
