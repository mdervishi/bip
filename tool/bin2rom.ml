open Printf

let print_byte (tab : bytes) ofs (n : int) =
  let c = ref 0 in
  let n = ref n in
  while !n > 0 do
    Bytes.set tab (31 - (ofs + !c)) (if (!n land 1) = 1 then '1' else '0');
    incr c;
    n := !n / 2;
  done

let reset instr =
  for i = 0 to Bytes.length instr - 1 do Bytes.set instr i '0' done

let from_asm =
  if Array.length Sys.argv < 2 then begin
      eprintf "usage: %s <riscv-code> <dst.rom>\n" Sys.argv.(0);
      exit 1
    end;

  let ic = open_in_bin Sys.argv.(1) in
  let oc = open_out Sys.argv.(2) in
  let i = ref 0 in
  fprintf oc "rinstr+\n";
  let instr = Bytes.make 32 '0' in
  try while true do
        print_byte instr (!i * 8) (input_byte ic);
        incr i;
        if !i mod 4 = 0
        then begin
            fprintf oc "%s " (Bytes.unsafe_to_string instr);
            reset instr;
            i := 0;
          end
      done
  with End_of_file -> fprintf oc "\n"; close_in ic; close_out oc

let _ = from_asm
