SOURCE=src/alu.mj src/control.mj src/instruction.mj src/main.mj src/ram.mj src/register.mj src/const.mj src/util.mj

.PHONY: preprocess test tool clean

all: out.exe

tool:
	$(MAKE) -C ./tool/ all

out.mj: $(SOURCE)
	cpp src/main.mj > out.mj

out.net: tool out.mj
	if ! ./tool/mjc.byte -S out.mj ; then rm -f out.net ; fi

%.rom: %.s tool
	./tool/riskv2rom.sh $< $@

preprocess: out.mj

test: tests/list.txt tool out.mj
	./tests/test.sh

prg.s: asm/horloge.S asm/digit_gen.S
	cpp asm/horloge.S prg.s

out.exe: out.net prg.rom tool tool/main.cpp tool/afficheur.cpp tool/afficheur.h
	./tool/sim.byte -O -r prg.rom -f out.net
	g++ -Wall --std=gnu++11 -no-pie -O3 -o out.exe out.net.s tool/main.cpp tool/afficheur.cpp -lncurses

clean:
	make -C ./tool/ clean
	rm -f *.net *.exe
	rm -f *.cm[io] *.c *.o *.out
	rm -f *.byte
	rm -f prg.*
	rm -f out.*
