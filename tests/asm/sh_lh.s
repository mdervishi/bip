    .section    .text
    .globl      main
main:
    add     x1, x0, 7
    sll     x1, x1, 0x3f # address start with 1
    addi    x2, x1, 10   # store at address 10
    addi    x3, x0, 0x2a # store value 42
    lui     x3, 0xffff   # add some bits that should be ignored
    sh      x3, 0(x2)
    lh      x4, 0(x2)
    add     x0, x4, x0
