#!/bin/bash

cd tests/
sim="../tool/sim.byte -p 1 "
mj="../tool/mjc.byte -S "
cp "../out.mj" "./out.mj"
src_mj="./out.mj"
src_net="./out.net"
out=".out"
while read -r line; do
    main=`cut -d ';' -f 1 <<< $line`
    in=`cut -d ';' -f 2 <<< $line`
    eout=`cut -d ';' -f 3 <<< $line`
    $mj -m "$main" "$src_mj"
    $sim "$src_net" < "$in" > "$out"
    if ! cmp -s "$eout" "$out" ;then
        echo "echec sur $main :: $in"
        exit 1
    fi
done < list.txt
rm -f "$out"
exit 0
