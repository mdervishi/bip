Rapport du projet de simulation numérique
========================================

*bip* est un processeur basé sur l'architecture RISC-V 64 bits conçu par Benjamin Bonneau, Tom Hubrecht, Megi Dervishi et Maxime Flin.

## Les instructions
Nous avons choisit d'implémenter l'ensemble des instructions entières de base du processeur 32 bit (sauf `FENCE` qui est sans effet) et l'extension 64 bits.
De plus, nous projetons de faire la multiplication d'entiers de l'extension `RV64M Standard Extension`.

Les entiers sont représentés en gros-boutiste.

## RAM et périphériques
Le processeur dispose d'une RAM et d'un accès à des périphériques en écriture (affichage 7 segments) et en lecture (horloge externe).
L'accès à ces périphériques est associé à une plage d'adresse.

La valeur associée à l'horloge externe est maintenue à 1 tant que le programme ne la lit pas.

## ALU
L'additionneur implémenté est le carry-lookahead adder
